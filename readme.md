<p align="center"><a href="https://laravel.com" target="_blank"><img width="150"src="https://laravel.com/laravel.png"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

<h1>Video Gallery</h1>



    Table list:
    1.	Users
        a.	Admin
        b.	Normal users
    2.	User Profiles
    3.	Videos
    4.	Video description
    5.	Galleries
    6.	Categories
    7.	Comments
    8.	Tag
    9.	Views
    10.	Comments_like
    11.	video_like
    12.	Permissions
    13.	Modules
    14.	Report
        1.a	Admin:
                        I.	Id (primary key)
                        II.	Profile id (Foreign key form profiles table by default null)
                        III.	User name
                        IV.	Email address
                        V.	Password
                        VI.	User level
                        VII.	Varfication_url
                        VIII.	Verify email
                        IX.	Crated at
                        X.	Updated at
                        XI.	Deleted at

        1.b Normal Users:
                        I.	Id (primary key)
                        II.	Profile id (Foreign key form profiles table by default null)
                        III.	User name
                        IV.	Email address
                        V.	Password
                        VI.	Verify email
                        VII.	Crated at
                        VIII.	Updated at
                        IX.	Deleted at

        2 user profiles (share both users):
                        I.	Id (primary key)
                        II.	First name
                        III.	Last Name
                        IV.	Mobile no
                        V.	Gender
                        VI.	Address
                        VII.	link
                        VIII.	short description
                        IX.	profile image
                        X.	Crated at
                        XI.	Updated at
                        XII.	Deleted at
                        
        3 videos:
                        I.	Id (primary key)
                        II.	User_id (foreign key form users table)
                        III.	Title
                        IV.	Path
                        V.	Ex_link (path from other site url)
                        VI.	Gallery_id (foreign key form galleries table)
                        VII.	Category_ id (foreign key form categories table)
                        VIII.	Privacy (by default private)
                        IX.	Crated at
                        X.	Updated at
                        XI.	Deleted at
                        
        4.  video_descriptions:
                        I.	Id (pk)
                        II.	Video_id (Foreign key from videos table)
                        III.	Description
                        IV.	Others
                        V.	Crated at
                        VI.	Updated at
                        VII.	Deleted at
                        
        5. Galleries:
                        I.	Id (primary key)
                        II.	User_id(Foreign key from users table)
                        III.	Video_id (Foreign key from videos table)
                        IV.	Title
                        V.	Description
                        VI.	Crated at
                        VII.	Updated at
                        VIII.	Deleted at
                        
        6. Categories:
                        I.	Id (Primary key)
                        II.	Admin_id (Foreign key from admin table)
                        III.	Title
                        IV.	Description
                        V.	Crated at
                        VI.	Updated at
                        VII.	Deleted at
                        
        7. Comments:
                        I.	Id (Primary key)
                        II.	User_id (Foreign key from users table)
                        III.	comments
                        IV.	Replay_id(Foreign key from same table)
                        V.	Comments_path (file/url)
                        VI.	Crated at
                        VII.	Updated at
                        VIII.	Deleted at
                        
        8. Tags:
                        I.	Id (Primary key)
                        II.	Video_id (Foreign key)
                        III.	gallery_id (Foreign key)
                        IV.	Crated at
                        V.	Updated at
                        VI.	Deleted at

        9.	Views:

                        I.	Id (primary key)
                        II.	User_id (Foreign key)
                        III.	Video_id (Foreign key)
                        IV.	Crated at
                        V.	Updated at
                        VI.	Deleted at
                        
        10.	Comments_like:
                        i.	Id (primary key)
                        ii.	User_id (Foreign key)
                        iii.	Comment_id(Foreign key)
                        iv.	Likes
                        v.	Dislikes
                        vi.	Crated at
                        vii.	Updated at
                        viii.	Deleted at
                        
        11.	video_like:
                        i.	Id (primary key)
                        ii.	User_id (Foreign key)
                        iii.	video_id(Foreign key)
                        iv.	Likes
                        v.	Dislikes
                        vi.	Crated at
                        vii.	Updated at
                        viii.	Deleted at
                        
        12.	Permission:
                        i.	Id (primary key)
                        ii.	User_id
                        iii.	Module_id
                        iv.	Add
                        v.	Edit
                        vi.	Delete
                        vii.	Crated at
                        viii.	Updated at
                        ix.	Deleted at
                        
        13.	Modules:
                        i.	Id (primary key)
                        ii.	Name
                        iii.	Crated at
                        iv.	Updated at
                        v.	Deleted at

        14.	Reports: 
                        i.	Id(primary Key)
                        ii.	Video_id
                        iii.	User_id
                        iv.	Reports
                        v.	Description
                        vi.	Crated at
                        vii.	Updated at
                        viii.	Deleted at

